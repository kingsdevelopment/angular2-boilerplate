import {Component} from 'angular2/core';
import {AppState} from '../app.service';

@Component({
  selector: 'about',
  template: require('./about.jade')
})
export class About {

  constructor(public appState: AppState) {}

}
