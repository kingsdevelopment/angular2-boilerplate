import {RouteDefinition} from 'angular2/router';
import {Home} from './home';

export const Routes: RouteDefinition[] = [
  { path: '/', name: 'Home', component: Home, useAsDefault: true },
  
  // // Async load a component using Webpack's require with es6-promise-loader and webpack `require`
  { path: '/about', name: 'About', loader: () => require('es6-promise!./about')('About') }
];