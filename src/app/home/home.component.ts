import {Component} from 'angular2/core';
import {AppState} from '../app.service';

@Component({
  selector: 'home',
  template: require('./home.jade')
})
export class Home {

  constructor(public appState: AppState) {}

}
